import {ADD_PRODUCT, INIT_PRODUCT, REMOVE_PRODUCT} from "../action/action";
import hamburger from "../../components/images/hamburger.png";
import cheeseburger from "../../components/images/cheeseburger.png";
import fries from "../../components/images/fries.jpg";
import coffee from "../../components/images/coffee.jpeg";
import tea from "../../components/images/tea.jpg";
import coke from "../../components/images/coke.jpg";

const INITIAL_PRODUCT = {
    hamburger: 0,
    cheeseburger: 0,
    coffee: 0,
    tea: 0,
    cola: 0,
    fries: 0
};

const PRODUCT_PRICES = {
    products: [
        { name: "Hamburger", price: 80, img: hamburger },
        { name: "Cheeseburger", price: 90, img: cheeseburger},
        { name: "Fries", price: 45, img: fries},
        { name: "Coffee", price: 70, img: coffee },
        { name: "Tea", price: 50, img: tea},
        { name: "Cola", price: 40, img: coke}
    ],
    deliver: 150,
};

const initialState = {
    products: {...PRODUCT_PRICES},
    order: {...INITIAL_PRODUCT},
    deliver: PRODUCT_PRICES.deliver,
};

const reducer = (state = initialState, action) => {

    switch(action.type) {
        case ADD_PRODUCT:
            return {
                ...state,
                order: {
                    ...state.order,
                    [action.productName.toLowerCase()]: state.order[action.productName.toLowerCase()] + 1
                },
                totalPrice: state.totalPrice + PRODUCT_PRICES[action.productName]
            };

        case REMOVE_PRODUCT:
            if (state.order[action.productName] > 0) {
                return {
                    ...state,
                    order: {
                        ...state.order,
                        [action.productName.toLowerCase()]: state.order[action.productName.toLowerCase()] - 1
                    },
                    totalPrice: state.totalPrice - PRODUCT_PRICES[action.productName]
                };
            }
            return state;

        case INIT_PRODUCT:
            return {
                products: {...PRODUCT_PRICES},
                order: {...INITIAL_PRODUCT},
                deliver: PRODUCT_PRICES.deliver,
            };

        default:
            return state;
    }
};

export default reducer;