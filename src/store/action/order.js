import axios from '../../axios-order';

import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./action";

export const orderRequest = () => ({type: ORDER_REQUEST});

export const orderSuccess = () => ({type: ORDER_SUCCESS});

export const orderFailure = error => ({type: ORDER_FAILURE, error});


export const createOrder = order => {
    return dispatch => {
        dispatch(orderRequest());

        axios.post('/products.json', order).then(
            response => dispatch(orderSuccess()),
            error => dispatch(orderFailure(error))
        );
    }
};