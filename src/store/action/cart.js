import {ADD_PRODUCT, INIT_PRODUCT, REMOVE_PRODUCT} from "./action";

export const addIngredient = productName => {
    return {type: ADD_PRODUCT, productName}
};

export const removeIngredient = productName => {
    return {type: REMOVE_PRODUCT, productName}
};

export const product = () => {
    return {type: INIT_PRODUCT}
};