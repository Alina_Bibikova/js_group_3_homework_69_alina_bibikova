import React, { Component } from 'react';
import './App.css';
import {Switch, Route} from "react-router-dom";
import Product from "./containers/Product/Product";
import ContactData from "./containers/ContactData/ContactData";

class App extends Component {

  render() {
    return (
         <Switch>
            <Route path="/" exact component={Product} />
            <Route path="/checkout/contact-data" component={ContactData} />
         </Switch>
    );
  }
}

export default App;
