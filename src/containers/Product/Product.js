import React, { Component } from 'react';
import Cart from "../../components/Cart/Cart";
import ProductsList from "../../components/ProductList/ProductList";
import {connect} from "react-redux";
import {addIngredient, removeIngredient} from "../../store/action/cart";

class Product extends Component {
    state = {
        disabled: 'disabled'
    };

    addIngredient = (name) => {
        this.props.onIngredientAdded(name);
        this.setState({disabled : 'enabled'})
    };

    // removeIngredient = (name) => {
    //     this.props.onIngredientRemove(name);
    //     this.setState({disabled : 'disabled'})
    // };

    purchaseContinue = () => {
        if (this.state.disabled === 'enabled'){
            this.props.history.push('/checkout/contact-data');
        }
    };

    getOrderList = () => {
        const order  = this.props.order;
        const products = this.props.products;

            return Object.keys(order).map((name, id) => {
                return {name, deliver: products.deliver, price:products.products[id].price, quantity: order[name]};
            });
    };

    render() {
        return (
            <div className="container">
                <div className="constructor">
                    <div className="constructor__products">
                        <h2>Our products</h2>
                        <ProductsList
                            items={this.props.products}
                            onAddClick={this.addIngredient}
                        />
                    </div>
                    <div className="constructor__order">

                        <h2>Order</h2>
                        <Cart items={this.getOrderList()}
                              onRemoveClick={this.props.onIngredientRemove}
                              deliver={this.props.deliver}
                              purchaseContinue={this.purchaseContinue}
                              disabled={this.state.disabled}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    products: state.bb.products,
    deliver: state.bb.products.deliver,
    order: state.bb.order,
});

const mapDispatchToProps = dispatch => ({
    onIngredientAdded: productName => dispatch(addIngredient(productName)),
    onIngredientRemove: productName => dispatch(removeIngredient(productName))
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
