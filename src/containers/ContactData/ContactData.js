import React, {Component} from 'react';
import axios from '../../axios-order';
import {connect} from "react-redux";
import {product} from "../../store/action/cart";
import './ContactData.css';

class ContactData extends Component {
    state = {
        name: '',
        email: '',
        street: '',
        postal: '',
        loading: false
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    orderHandler = event => {
        event.preventDefault();

        this.setState({loading: true});

        const order = {
            order: this.props.order,
            customer: {
                name: this.state.name,
                email: this.state.email,
                street: this.state.street,
                postal: this.state.postal
            }
        };

        axios.post('/products.json', order).finally(() => {
            this.props.initIngredients();
            this.setState({loading: false});
            this.props.history.push('/');
        });
    };

    render() {

        let form = (

            <form onSubmit={(e) => this.orderHandler(e)}>

                <input className="Input" type="text" name="name" placeholder="Your Name"
                       value={this.state.name} onChange={this.valueChanged}
                />
                <input className="Input" type="email" name="email" placeholder="Your Mail"
                       value={this.state.email} onChange={this.valueChanged}
                />

                <input className="Input" type="text" name="street" placeholder="Street"
                       value={this.state.street} onChange={this.valueChanged}
                />

                <input className="Input" type="text" name="postal" placeholder="Postal Code"
                       value={this.state.postal} onChange={this.valueChanged}
                />

                <button className="Order">ORDER</button>
            </form>
        );

        if (this.state.loading) {
            form = <p>Loading....</p>
        }

        return (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    order: state.bb.order,
});

const mapDispatchToProps = dispatch  => ({
    initIngredients: () => dispatch(product())
});

export default connect (mapStateToProps, mapDispatchToProps) (ContactData);
