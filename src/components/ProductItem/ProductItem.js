import React from "react";
import "./ProductItem.css";

const ProductItem = ({ onAddClick, name, price, img }) => (
    <div className="product-item">
        <h3 className="product-item__title">{name}</h3>
        <div className="product-item__price">Price: {price} KGS</div>
        <img className="img" src={img} />
        <button
            type="button"
            className="button button_color_aqua product-item__add"
            onClick={() => onAddClick(name)}
        >
            Order to cart
        </button>
    </div>
);

export default ProductItem;