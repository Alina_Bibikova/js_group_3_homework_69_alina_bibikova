import React from "react";
import ProductItem from "../ProductItem/ProductItem";
import './ProductList.css';

const ProductsList = ({ onAddClick, items }) => {
    return (
        <ul className="products-list">
            {items.products.map(item => (
                <li className="products-list__item" key={item.name}>
                    <ProductItem {...item}
                                 onAddClick={onAddClick}
                    />
                </li>
            ))}
        </ul>
    );
};

export default ProductsList;