import React from 'react';

const Button = props => {
    return (
        <div>
            <button className={"PlaceOrder ".concat(props.disabled)} onClick={props.cliked}>Place order</button>
        </div>
    );
};

export default Button;