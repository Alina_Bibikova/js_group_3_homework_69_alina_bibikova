import React from "react";
import OrderItem from "../OrderItem/OrderItem";
import "./OrderList.css";

const OrderList = ({ items, onRemoveClick }) => {
    return (
        <ul className="order-list">
            {items.map(item => (
                <li className="order-list__item"
                    key={item.name}>
                    <OrderItem {...item}
                               onRemoveClick={() => onRemoveClick(item.name)} />
                </li>
            ))}
        </ul>
    );
};

export default OrderList;