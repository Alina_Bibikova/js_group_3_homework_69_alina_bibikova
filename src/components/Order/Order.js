import React from "react";
import './Order.css';
import OrderList from "../OrderList/OrderList";
import OrderTotal from "../OrderTotal/OrderTotal";

const Order = props => {
    const addReducer = (accumulator, currentValue) => accumulator + currentValue;
    const total = props.items
        .map(item => item.price * item.quantity)
        .reduce(addReducer, 0);

    return (
        <div className="order">
            <div className="order__list">
                <OrderList {...props} />
            </div>
            <div className="order__total">
                <OrderTotal disabled={props.disabled} cliked={props.purchaseContinue} deliver={props.deliver} total={total} />
            </div>
        </div>
    );
};

export default Order;