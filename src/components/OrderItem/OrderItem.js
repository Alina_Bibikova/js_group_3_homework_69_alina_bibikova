import React from "react";
import "./OrderItem.css";

const OrderItem = ({ name, quantity, price, onRemoveClick}) => {
    return (
        <div className="order-item">
            <h3 className="order-item__title">{name}</h3>
            <div className="order-item__right">
                <div className="order-item__quantity">&times; {quantity}</div>
                <div className="order-item__price">{price * quantity} KGS</div>
                <div className="order-item__remove">
                    <button
                        type="button"
                        className="button button_color_red"
                        onClick={onRemoveClick}
                    >
                       -
                    </button>
                </div>
            </div>
        </div>
    );
};

export default OrderItem;