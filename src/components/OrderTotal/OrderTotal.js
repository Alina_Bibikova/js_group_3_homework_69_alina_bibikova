import React from "react";
import "./OrderTotal.css";
import Button from "../UI/Buttun/Button";

const OrderTotal = ({ total, deliver, cliked, disabled }) => {
    return (
        <div className='container'>
            <div className="order-deliver">
                <span className="order-deliver__label">Deliver:</span>
                <span className="order-deliver__value"> {deliver} KGS</span>
            </div>
            <div className="order-total">
                <span className="order-total__label">Total price: </span>
                <span className="order-total__value"> {total + deliver} KGS</span>
            </div>
            <div>
               <Button disabled={disabled} cliked={cliked}/>
            </div>
        </div>
    );
};

export default OrderTotal;