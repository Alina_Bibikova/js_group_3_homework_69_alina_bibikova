import React from "react";
import Order from "../Order/Order";

const Cart = props => {
    return (
        <div className="cart">
                <Order {...props} />
        </div>
    );
};

export default Cart;